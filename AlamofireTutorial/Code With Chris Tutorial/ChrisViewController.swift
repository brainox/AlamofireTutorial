//
//  ChrisViewController.swift
//  AlamofireTutorial
//
//  Created by Decagon on 27/07/2021.
//

import UIKit
import Alamofire

class ChrisViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        AF.request("https://codewithchris.com/code/afsample.json").response { response in
//            debugPrint(response)
//        }
        AF.request("https://codewithchris.com/code/afsample.json").responseJSON { (response) in
            if let error = response.error {
                print(error.localizedDescription)
            } else if let jsonValue = response.value as? [String: Any] {
                print(jsonValue)
                print(jsonValue["firstkey"] as! String)
                print(jsonValue["secondkey"] as! NSArray)
                
                
            }
        }
    }

}
