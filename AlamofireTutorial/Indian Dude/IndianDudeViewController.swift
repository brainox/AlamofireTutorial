//
//  IndianDudeViewController.swift
//  AlamofireTutorial
//
//  Created by Decagon on 27/07/2021.
//

import UIKit

class IndianDudeViewController: UIViewController {

    @IBOutlet weak var countriesTableView: UITableView!
    var countries = [Country]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countriesTableView.dataSource = self
        let service = Service(baseUrl: "https://restcountries.eu/rest/v2/")
        service.getAllCountryNamesFrom(endpoint: "all")
        service.completionHandler { [weak self] countries, status, message in
            if status {
                guard let self = self else {return}
                guard let _countries = countries else {return}
                self.countries = _countries
                self.countriesTableView.reloadData()
            }
        }
    }

}
extension IndianDudeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = countriesTableView.dequeueReusableCell(withIdentifier: "countryCell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "countryCell")
        }
        let country = countries[indexPath.row]
        cell?.textLabel?.text = (country.name ?? "") + " " + (country.countryCode ?? "")
        cell?.detailTextLabel?.text = country.capital ?? ""
        return cell!
    }
    
}
