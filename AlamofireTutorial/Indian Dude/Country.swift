//
//  Country.swift
//  AlamofireTutorial
//
//  Created by Decagon on 27/07/2021.
//

import Foundation

struct Country: Decodable {
    var name: String?
    var capital: String?
    var countryCode: String?
    
    enum CodingKeys: String, CodingKey{
        case name = "name"
        case capital = "capital"
        case countryCode = "alpha3Code"
    }
}
