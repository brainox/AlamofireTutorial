//
//  Service.swift
//  AlamofireTutorial
//
//  Created by Decagon on 27/07/2021.
//

import Foundation
import Alamofire

class Service {
    fileprivate var baseUrl = ""
    typealias countriesCallBack = (_ countries: [Country]?, _ status: Bool, _ message: String) -> Void
    var callBack: countriesCallBack?
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func getAllCountryNamesFrom(endpoint: String) {
        AF.request(self.baseUrl + endpoint, method: .get, encoding: URLEncoding.default).response { (responseData) in
            guard let data = responseData.data else {
                self.callBack?(nil, false, "")
                return
            }
            do {
                let countries = try JSONDecoder().decode([Country].self, from: data)
                self.callBack?(countries, true, "")
            } catch {
                self.callBack?(nil, false, error.localizedDescription)
            }
        }
    }
    
    func completionHandler(_callBacK: @escaping countriesCallBack) {
        self.callBack = _callBacK
    }
}
